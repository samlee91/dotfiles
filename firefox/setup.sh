#! /usr/bin/env zsh

if [ ! -d $HOME/.mozilla ]; then
   print "no firefox install. Installing"
   print "setup a user account at about:profiles in firefox"
   sudo pacman -S firefox
   exit 1
fi


profile_dir=$(find $HOME/.mozilla/firefox -type d -name "*$USER*")

[ -d $profile_dir/chrome ] && rm -rf $profile_dir/chrome
[ -f $profile_dir/user.js ] && rm $profile_dir/user.js

ln -s $HOME/.dotfiles/firefox/chrome $profile_dir/chrome
ln -s $HOME/.dotfiles/firefox/user.js $profile_dir/user.js

