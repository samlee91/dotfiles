#! /usr/bin/env zsh

dependencies=(
   bluez
   bluez-utils
   bluez-tools
   pulseaudio-bluetooth
)

sudo pacman -S --needed --noconfirm $dependencies
#yay -S --needed rtl88x2bu-dkms-git

if [[ -z $(lsmod | grep btusb) ]]; then
   modprobe btusb
fi

if [ $(sudo systemctl is-active bluetooth) = "inactive" ]; then
   sudo systemctl start bluetooth.service
   sudo systemctl enable bluetooth.service
fi


sed -i 's/#\s*AutoEnable\s*=/AutoEnable=/' /etc/bluetooth/main.conf
