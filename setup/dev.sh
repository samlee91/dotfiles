#! /usr/bin/env zsh

language_packages=(
   neovim
   julia
   python3
   r
   rustup
   jre-openjdk
   jdk-openjdk
   lua
)

lsp_packages=(
   bash-language-server
   ccls
   rust-analyzer
   pyright
   lua-language-server
   texlab
)

aur_lsp_packages=(
   cssmodules-language-server
)

sudo pacman -S --needed --noconfirm $language_packages $lsp_packages
yay -S --needed --noconfirm $aur_lsp_packages

# julia
julia -e '
   using Pkg
   Pkg.add("LanguageServer")
   Pkg.add("SymbolServer")
   Pkg.add("OhMyREPL")
   Pkg.add("VimBindings")
   Pkg.add("Crayons")
   Pkg.add("ImageIO")
   Pkg.add("FileIO")
'

luarocks install inspect
