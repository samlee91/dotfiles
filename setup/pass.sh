#! /usr/bin/env zsh
source ~/.dotfiles/dotconfig/startup/envivars.sh

# gpg keys
function generate_key() {
    secret_key=$(gpg --list-secret-keys \
                     --keyid-format LONG \
               | grep -oP "(?<=rsa\d{4}/).*(?=\s\d{4}-\d{2}-\d{2}\s\[SC\])")

    if [ -z "$secret_key" ]; then
        gpg --full-generate-key
    fi
}

# pass
source ~/.dotfiles/dotconfig/startup/envivars.sh
sudo pacman -S --needed --noconfirm pass

if [ ! -d $GNUPGHOME ]; then
    mkdir -p $GNUPGHOME
fi

if [ ! -d $PASSWORD_STORE_DIR ]; then
    mkdir -p $PASSWORD_STORE_DIR
    mykey=$(generate_key)
    cd $PASSWORD_STORE_DIR && pass init $mykey
fi
