#! /usr/bin/env zsh

pacman_packages=(
   kicad
   kicad-library
   kicad-library-3d
)

sudo pacman -S --needed --noconfirm $pacman_packages
