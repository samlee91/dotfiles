#! /usr/bin/env zsh

pacman_packages=(
   gtk2
   gtk3
   gtk4
   hunspell-en_us
   asciiquarium
   cmatrix
   arc-gtk-theme
   dunst
   feh
   firefox
   flameshot
   inkscape
   obs-studio
   libreoffice-still-en-gb
   pamixer
   pcmanfm
   polybar
   playerctl
   rofi
   sxiv
   vim
   zathura
   zathura-pdf-poppler
)

aur_packages=(
   bibata-cursor-theme 
   candy-icons-git
   picom-jonaburg-git
   sioyek
   sc-im
   xdg-ninja
   pipes.sh
)


sudo pacman -S --needed --noconfirm
sudo pacman -S --needed --noconfirm flatpak $pacman_packages
yay -S --needed --noconfirm $aur_packages

# default applications
case $1 in
    "-m" | "--mime")
        xdg-mime default org.pwmt.zathura-pdf-poppler.desktop application/pdf
        ;;
    "*")
        ;;
esac
