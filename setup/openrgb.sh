#! /usr/bin/env zsh

packages=(
   openrgb
)

dependencies=(
   openal
)

plugins=(
   "https://gitlab.com/OpenRGBDevelopers/OpenRGBEffectsPlugin/-/jobs/4632324432/artifacts/download"
)

[ ! -d tmp ] && mkdir tmp
sudo pacman -S --needed --noconfirm $dependencies $packages
curl -L ${plugins[1]} -o tmp/OpenRGBEffectsPlugin.zip

