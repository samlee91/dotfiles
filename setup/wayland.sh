#! /usr/bin/env zsh

pacman_packages=(
   slurp
   sway hyprland
   swaybg
   grim
   imv
   wofi
   fuzzel
   fnott
   wl-clipboard
   xorg-xwayland
   xorg-xlsclients
   xdg-desktop-portal-hyprland
   qt5-wayland
   glfw-wayland
)

aur_packages=(
   eww-wayland
   wlr-randr
   wev
)

sudo pacman -S --needed --noconfirm $pacman_packages
yay -S --needed --noconfirm $aur_packages

# uninstall xdg-desktop-portal; makes firefox launch very slow
