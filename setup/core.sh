#! /usr/bin/env zsh

community_packages=(
   bat
   beets
   btop
   lolcat
   exa
   evtest
   dosfstools
   figlet
   fzf
   gettext
   git
   gotop
   glow
   imagemagick
   kitty
   linux-headers
   man-db
   man-pages
   net-tools
   neovim
   pass
   pdftk
   ripgrep
   screen
   texlive-binextra  # pdfcrop
   tokei
   tldr
   trash-cli
   usbutils
   unzip
   wtype
   xdo
   xdotool
   xsel
   xclip
   xorg-xev
)

aur_packages=(
   amdgpu-fan
   bat-extras
   cava
   cli-visualizer
   openrgb
   pfetch-rs
   s-tui
   timeshift
   textext
   units
)


# set up directory for building software from source
BUILD=/opt/build
if [ ! -d $BUILD ]; then
   sudo mkdir -p $BUILD
   sudo chown $USER $BUILD
fi

# build yay
if [ ! -d $BUILD/yay ]; then
   cd $BUILD && {
      git clone https://aur.archlinux.org/yay.git
      cd yay && makepkg -si
   }
fi

sudo pacman -S --needed --noconfirm $community_packages
yay -S --needed --noconfirm $aur_packages
