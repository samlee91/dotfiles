#! /usr/bin/env zsh

distro="$(cat /etc/*release | grep -oP "(?<=^ID=).*(?=$)")"


if [[ "$distro" = *arch* ]]; then
   _install="sudo pacman -S --needed --noconfirm "

elif [[ "$distro" = *ubuntu* ]]; then
   _install="sudo apt install -y "

elif [[ "$distro" = *fedora* ]]; then
   _install="sudo dnf install -y "

else
   echo "unknown distro. install command not determined."
   return
fi


eval "${_install}" libretro-mesen-s
