#! /usr/bin/env zsh


# install yay
if [ ! -d ~/.yay ]; then
   print -P "%F{magenta}::%f Installing yay package manager"
   git clone https://aur.archlinux.org/yay.git ~/.yay
   { cd ~/.yay && makepkg -si }
fi

# clone my repos
source ~/.dotfiles/dotconfig/startup/envivars.sh
mygitlab="https://gitlab.com/samlee91"
[ ! -d $XDG_CONFIG_HOME/nvim ] && git clone $mygitlab/nvim.git ~/.config/nvim
[ ! -d $XDG_CONFIG_HOME/zsh ]  && git clone $mygitlab/zupershell.git ~/.config/zsh


# setup scripts
source ~/.dotfiles/fresh_install/core_setup.sh
source ~/.dotfiles/fresh_install/desktop_setup.sh
source ~/.dotfiles/fresh_install/printer_setup.sh
source ~/.dotfiles/fresh_install/wayland_setup.sh

{ cd ~/.dotfiles/dotconfig/startup && ./setup.sh }
{ cd ~/.dotfiles/fonts && ./setup.sh }
