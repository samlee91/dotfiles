#! /usr/bin/env zsh

sudo pacman -S --needed --noconfirm ttf-fira-code ttf-nerd-fonts-symbols ttf-joypixels
yay -S --needed --noconfirm ttf-hack-ligatured


if [ -z "$(find /usr/share/fonts -name 'Victor*')"  ]; then
   cd ~/.dotfiles/fonts && {
      mkdir tmp
      unzip VictorMonoAll.zip -d tmp
      sudo cp tmp/TTF/* /usr/share/fonts/TTF
      rm -rf tmp
   }
fi

