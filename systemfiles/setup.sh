#! /usr/bin/env zsh

function relink() {
   source_file=$1
   dest_file=$2
   rm_cmd='rm -rf $dest_file'
   ln_cmd='ln -s $source_file $dest_file'

   if [[ $dest_file != $HOME/* ]]; then
      rm_cmd='sudo rm -rf $dest_file'
      ln_cmd='sudo ln -s $source_file $dest_file'
   fi

   if [ -d $dest_file ] || [ -f $dest_file ] || [ -L $dest_file ]; then
      eval ${rm_cmd}
   fi

   eval ${ln_cmd}
}


source ~/.dotfiles/dotconfig/startup/envivars.sh

[ ! -d $GNUPGHOME ] && mkdir -p $GNUPGHOME
relink ~/.dotfiles/systemfiles/gpg-agent.conf $GNUPGHOME/gpg-agent.conf
relink ~/.dotfiles/systemfiles/pacman.conf /etc/pacman.conf
sudo cp ~/.dotfiles/systemfiles/sudoers /etc/sudoers
sudo cp ~/.dotfiles/systemfiles/loader.conf /boot/loader/loader.conf

