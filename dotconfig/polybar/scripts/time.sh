#! /usr/bin/env zsh


case $1 in

   'time')
      hour=$(date +%I)
      minute=$(date +%M)
      
      if [ $hour[1] = "0" ]; then
         hour=$hour[2]
      fi
      print $hour:$minute
      ;;

   'date')
      day=$(date +%A)
      date=$(date +%D)
      print $day,$date
      ;;
esac
      

