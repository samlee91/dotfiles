#! /usr/bin/env zsh


cpu_usage=($(mpstat | grep all | cut -d \  -f 5-))

(( total_cpu = 0 ))
for (( i = 0; i < 10; i++ )); do
   (( total_cpu += cpu_usage[i] ))
done

dunstify "CPU: $total_cpu[1]$total_cpu[2]$total_cpu[3]$total_cpu[4]%"  -t 3000
