#! /usr/bin/env zsh

killall -q polybar

# Wait until the processes have been shut down
while pgrep -u $UID -x polybar > /dev/null; do
   sleep 1
done


# Determine window manager
case $DESKTOP_SESSION in

   i3 | i3-with-shmlog)
      config="i3-minimal"
      ;;

   bspwm)
      config="bspwm_floating"

      ;;
   *)
      config="default"
      ;;
esac


if [ $COMPUTER = "laptop" ]; then
   config="$config-laptop"
fi

config="~/.config/polybar/$config.config"
#config="~/.config/polybar/bspwm_floating.config"

# Launch bars
for m in $(polybar --list-monitors | cut -d":" -f1); do
    MONITOR=$m polybar --reload main -c $config &
done
