#! /usr/bin/env zsh

#function get_active_process() {
#   hyprctl activewindow | grep -oP "(?<=class: ).*(?=$)"
#}

#process=$(get_active_process)
#icon=$(eww get ${process}_icon)

process=$(uname -r)
icon=$(eww get default_proc_icon)

echo "$icon$process"

#while [ 1 ]; do
#   new_process=$(hyprctl activewindow | grep -oP "(?<=class: ).*(?=$)")
#   if [ "$new_process" != "$process" ]; then
#      echo "$new_process"
#      process="$new_process"
#   fi
#done
