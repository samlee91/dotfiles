#! /usr/bin/env zsh

time_string=$(date +%I:%M)

# trim leading '0'
[ ${time_string[1]} = "0" ] && time_string=${time_string:1}
echo $time_string
