#! /usr/bin/env zsh

TIMEOUT=3
volume=${$(pamixer --get-volume-human)%\%*}
icon="volume_adjust_icon"

if [ "$volume" = "muted" ]; then
   volume=0
   icon="volume_muted_icon"
fi

eww update volume_level=$volume
eww update current_volume_icon=$(eww get $icon)
[ $1 = "init" ] && return

script_count=$(( 1 + $(eww get volume_change) ))
eww update volume_change=$script_count

(( script_count == 1 )) && eww open-many volume_popup_left volume_popup_right


# close after 3 seconds if volume unchanged
sleep $TIMEOUT
(( script_count != $(eww get volume_change) )) && return
eww close volume_popup_left volume_popup_right
eww update volume_change=0
