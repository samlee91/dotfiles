#! /usr/bin/env zsh

active_workspaces=( $(hyprctl monitors | grep -oP "(?<=\()[0-9]+(?=\))") )
active_icon=$(eww get active_icon)
inactive_icon=$(eww get inactive_icon)

for i in {1..10}; do
   if [[ $i == $active_workspaces[1] ]] || [[ $i == $active_workspaces[2] ]]; then
      eww update workspace${i}=$active_icon
   else
      eww update workspace${i}=$inactive_icon
   fi
done

