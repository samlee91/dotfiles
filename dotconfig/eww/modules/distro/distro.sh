#! /usr/bin/env zsh

distro=$(hostnamectl | grep -oP "(?<=Operating System: ).*(?=$)")
icon=$(eww get $(echo "$distro" | awk '{print tolower($1)}')_icon)

[ -n $distro ] && eww update distro=$distro
[ -n $icon ]   && eww update distro_icon=$icon
