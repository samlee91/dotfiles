#! /usr/bin/env zsh

if [ -z $(pidof eww) ]; then
   eww daemon
   wait
fi

source $XDG_CONFIG_HOME/eww/modules/workspaces/workspaces.sh
source $XDG_CONFIG_HOME/eww/modules/distro/distro.sh
source $XDG_CONFIG_HOME/eww/modules/volume/volume.sh init

eww open-many bar_0 bar_1
