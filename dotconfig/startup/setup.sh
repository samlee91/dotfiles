#! /usr/bin/env zsh

function relink() {
   source_file=$1
   dest_file=$2

   [ -d $dest_file ] && sudo rm -rf $dest_file
   [ -f $dest_file ] && sudo rm -rf $dest_file
   [ -L $dest_file ] && sudo rm -rf $dest_file

   sudo ln -s $source_file $dest_file
}


source ~/.dotfiles/dotconfig/startup/envivars.sh
relink ~/.dotfiles/dotconfig/startup/envivars.sh /usr/local/bin/envivars.sh
relink ~/.dotfiles/dotconfig/startup/startup.sh /usr/local/bin/startup.sh
relink ~/.dotfiles/dotconfig/startup/startw /usr/local/bin/startw
