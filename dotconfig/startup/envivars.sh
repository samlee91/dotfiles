#! /usr/bin/env zsh

# default programs
export TERMINAL=kitty
export EDITOR=nvim
export BROWSER=firefox
export PDF_VIEWER=zathura
export IMAGE_VIEWER=imv
export VIDEO_PLAYER=mpv
export MUSIC_PLAYER=cmus

# XDG variables
export XDG_CONFIG_HOME=$HOME/.config
export XDG_DATA_HOME=$HOME/.local/share
export XDG_STATE_HOME=$HOME/.local/state
export XDG_CACHE_HOME=$HOME/.cache

# shell utils
export ZDOTDIR=$XDG_CONFIG_HOME/zsh
export ZSH_PLUGINS=/usr/share/zsh/plugins
export MANPAGER="nvim +Man! -c ':setlocal showtabline=0' -c ':setlocal laststatus=0' -c ':setlocal foldcolumn=3'"

# files and directories
export KEYMAPS=$XDG_CONFIG_HOME/keebs/keymaps
export NVIMDIR=$XDG_CONFIG_HOME/nvim
export NVIM_PLUGINS=$HOME/.local/share/nvim/site/pack/packer/start
export LOCAL_NVIM_PLUGINS=$HOME/code/lua/nvim_plugins
export ENVIVARS=$XDG_CONFIG_HOME/startup/envivars.sh
export WALLPAPER=$HOME/.dotfiles/wallpapers/trees0.jpg
export BC_ENV_ARGS=$XDG_CONFIG_HOME/bc/config
export GRIM_DEFAULT_DIR=$HOME/Pictures/screenshots

# fonts
export FONT_SIZE=13
export CODE_FONT='Hack Nerd Font JBM Ligatured CCG'

# GTK
export GTK_THEME=Arc-Dark
export GTK_ICONS=candy-icons
export GTK_CURSOR=Bibata-Modern-Ice
export XCURSOR_THEME=Bibata-Modern-Ice

# openrgb
export MOBO_NAME="$(cat /sys/devices/virtual/dmi/id/board_name)"
export MOBO_RGB_COLOR=FFFFFF

# theme variables
source systemtheme --set onedark

# LaTeX
export LATEX_DIR=$HOME/math/LaTeX
export TEXMFHOME=$HOME/.local/share/texmf

# command line programs
export RIPGREP_CONFIG_PATH=$XDG_CONFIG_HOME/rg/rg.conf

# x11
export XINITRC=$XDG_CONFIG_HOME/X11/xinitrc
export XAUTHORITY=$XDG_RUNTIME_DIR/Xauthority

# xdg-ninja suggestions
export CARGO_HOME=$XDG_DATA_HOME/cargo
export PATH=$PATH:$CARGO_HOME/bin
export CUDA_CACHE_PATH=$XDG_CACHE_HOME/nv
export GTK2_RC_FILES=$XDG_CONFIG_HOME/gtk-2.0/gtkrc
export GNUPGHOME=$XDG_DATA_HOME/gnupg
export GOPATH=$XDG_DATA_HOME/go
export JULIA_DEPOT_PATH=$XDG_DATA_HOME/julia:$JULIA_DEPOT_PATH
export _JAVA_OPTIONS=-Djava.util.prefs.userRoot=$XDG_CONFIG_HOME/java
export HISTFILE=$XDG_STATE_HOME/bash/history
export LESSHISTFILE=$XDG_CACHE_HOME/less/history
export MYSQL_HISTFILE=$XDG_DATA_HOME/mysql_history
export PASSWORD_STORE_DIR=$XDG_DATA_HOME/pass
export PYTHONSTARTUP=$XDG_CONFIG_HOME/python/pythonrc
export PTPYTHON_CONFIG_HOME=$XDG_CONFIG_HOME/python/ptpython
export IPYTHONDIR=$XDG_CONFIG_HOME/python/ipython
export RUSTUP_HOME=$XDG_DATA_HOME/rustup
export TEXMFVAR=$XDG_CACHE_HOME/texlive/texmf-var
export W3M_DIR=$XDG_DATA_HOME/w3m
export NPM_CONFIG_USERCONFIG=$XDG_CONFIG_HOME/npm/npmrc

# wayland variables
export HYPRLAND_LOG_WLR=1
export WLR_NO_HARDWARE_CURSORS=1
export _JAVA_AWT_WM_NONEREPARENTING=1
export KITTY_ENABLE_WAYLAND=1
export HYPRLAND_LOG_WLR=1
export XDG_CURRENT_SESSION=Hyprland
export XDG_SESSION_DESKTOP=Hyprland
export XDG_SESSION_TYPE=wayland
export _JAVA_AWT_WM_NONEREPARENTING=1
#export GDK_BACKEND=wayland:x11
#export SDL_VIDEODRIVER=wayland
#export CLUTTER_BACKEND=wayland
#export QT_QPA_PLATFORM=wayland
#export QT_QPA_PLATFORMTHEME=gtk3
#export QT_QPA_PLATFORM="wayland;xcb"
#export QT_AUTO_SCREEN_SCALE_FACTOR=1
#export QT_WAYLAND_DISABLE_WINDOWDECORATION=1
