#! /usr/bin/env zsh

source /usr/local/bin/envivars.sh


sudo amdgpu-fan &
swaybg --image $WALLPAPER --mode fill &
$XDG_CONFIG_HOME/eww/launch.sh &

GSET="gsettings set org.gnome.desktop.interface"
$GSET gtk-theme $GTK_THEME
$GSET icon-theme $GTK_ICONS
$GSET cursor-theme $GTK_CURSOR

openrgb --device "$MOBO_NAME" --color $MOBO_RGB_COLOR &
