#! /usr/bin/env zsh


function relink() {
   source_file=$1
   dest_file=$2

   [ -e $dest_file ] && rm -rf $dest_file
   ln -s $source_file $dest_file
}





source ~/.dotfiles/dotconfig/startup/envivars.sh
config_dirs=( $(find ~/.dotfiles/dotconfig -type d) )

for config_dir in $config_dirs; do
   relink $config_dir $XDG_CONFIG_HOME/$(basename $dir)
done

