#! /usr/bin/env zsh

export PYTHONSTARTUP=$XDG_CONFIG_HOME/python/pythonrc
export IPYTHONDIR=~/.config/python/ipython

sudo pacman -S --needed --noconfirm ipython python-pygments

# this file causes errors in ipython
rm -rf ~/.local/lib/python3.11/site-packages/prompt_toolkit*
