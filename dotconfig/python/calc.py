from math import *
import fractions


def tofraction(decimal_value):
    ratio = fractions.Fraction(decimal_value).limit_denominator()
    print(type(ratio))


if __name__ == "__main__":
    print("\033[32mPress Ctrl+D to quit\033[0m")
