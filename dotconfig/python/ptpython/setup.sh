#! /usr/bin/env zsh

PYGMENT_STYLES_DIR=/usr/lib/python3.11/site-packages/pygments/styles
PTPYTHON_CONFIG_HOME=~/.config/python/ptpython

if [ ! -e $PYGMENT_STYLES_DIR/systemtheme.py ]; then
   sudo ln -s $PTPYTHON_CONFIG_HOME/systemtheme.py $PYGMENT_STYLES_DIR/systemtheme.py
fi

sudo pacman -S --needed --noconfirm python-tox
