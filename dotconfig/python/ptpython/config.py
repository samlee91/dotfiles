from prompt_toolkit.filters import ViInsertMode
from prompt_toolkit.key_binding.key_processor import KeyPress
from prompt_toolkit.keys import Keys
from prompt_toolkit.styles import Style
from ptpython.layout import CompletionVisualisation


__all__ = ["configure"]


def configure(repl):
    """
    Configuration method. This is called during the start-up of ptpython.

    :param repl: `PythonRepl` instance.
    """

    repl.vi_mode = True
    repl.cursor_shape_config = "Block"
    repl.show_signature = True
    repl.show_docstring = False
    repl.show_meta_enter_message = True
    repl.completion_visualisation = CompletionVisualisation.POP_UP # NONE, POP_UP, MULTI_COLUMN, TOOLBAR
    repl.completion_menu_scroll_offset = 0
    repl.show_line_numbers = False
    repl.show_status_bar = True
    repl.show_sidebar_help = True
    repl.swap_light_and_dark = False
    repl.highlight_matching_parenthesis = True
    repl.wrap_lines = True
    repl.enable_mouse_support = True
    repl.complete_while_typing = True
    repl.enable_fuzzy_completion = False
    repl.enable_dictionary_completion = False
    repl.paste_mode = False
    repl.prompt_style = "classic"  # 'classic' or 'ipython'
    repl.insert_blank_line_after_output = True
    repl.enable_history_search = True
    repl.enable_auto_suggest = True
    repl.enable_open_in_editor = True
    repl.enable_system_bindings = True
    repl.confirm_exit = False
    repl.enable_input_validation = True
    repl.color_depth = "DEPTH_8_BIT"  # The default, 256 colors.
    repl.min_brightness = 0.0
    repl.max_brightness = 1.0
    repl.enable_syntax_highlighting = True
    repl.vi_start_in_navigation_mode = True
    repl.vi_keep_last_used_mode = True
    repl.title = "My custom prompt."

    # Install custom colorscheme named 'my-colorscheme' and use it.
    #repl.install_ui_colorscheme(theme.YourStyle, Style.from_dict(_custom_ui_colorscheme))
    #repl.install_ui_colorscheme("my-colorscheme", Style.from_dict(_custom_ui_colorscheme))

    # Ptpython uses Pygments for code styling, so you can choose from Pygments'
    # color schemes. See:
    # https://pygments.org/docs/styles/
    # https://pygments.org/demo/
    #repl.use_code_colorscheme("native")
    repl.use_code_colorscheme("one-dark")


    # press 'Ctrl-C' twice to exit
    @repl.add_key_binding("c-c", "c-c")
    #@repl.add_key_binding("j", "j", filter=ViInsertMode())
    def _(event):
        event.cli.key_processor.feed(KeyPress(Keys.ControlD))
        #event.cli.current_buffer.insert_text("\nimport pdb; pdb.set_trace()\n")
        #event.current_buffer.validate_and_handle()
    # Custom key binding for some simple autocorrection while typing.

    #corrections = {
    #    "impotr": "import",
    #    "pritn": "print",
    #}

    #@repl.add_key_binding(" ")
    #def _(event):
    #    " When a space is pressed. Check & correct word before cursor. "
    #    b = event.cli.current_buffer
    #    w = b.document.get_word_before_cursor()

    #    if w is not None:
    #        if w in corrections:
    #            b.delete_before_cursor(count=len(w))
    #            b.insert_text(corrections[w])

    #    b.insert_text(" ")



# Custom colorscheme for the UI. See `ptpython/layout.py` and
# `ptpython/style.py` for all possible tokens.
#from os import environ
#_custom_ui_colorscheme = {
    #"prompt": f"bg:#ffffff  bold",
    #"prompt": f"bg:{environ['BLUE']} {environ['BLUE']} bold",
    # Make the status toolbar red.
    #"in.number": f"fg:{environ['ORANGE']} {environ['ORANGE']}",
    #"status-toolbar": "bg:#ff0000 #000000",
#}
