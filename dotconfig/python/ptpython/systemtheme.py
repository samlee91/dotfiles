from os import environ
from pygments.style import Style
from pygments.token import Comment, Keyword, Name, Number, Operator, \
    Punctuation, String, Token, Error, Generic


__all__ = ['SystemThemeStyle']


class SystemThemeStyle(Style):
    name = "systemtheme"
    background_color = environ["BACKGROUND"]

    styles = {
        Token: environ["WHITE"],

        Punctuation:         environ["YELLOW"],
        Punctuation.Marker:  environ["CYAN"],

        Keyword:              environ["PURPLE"],
        Keyword.Constant:     environ["PURPLE"],
        Keyword.Declaration:  environ["PURPLE"],
        Keyword.Namespace:    environ["PURPLE"],
        Keyword.Reserved:     environ["PURPLE"],
        Keyword.Type:         environ["PURPLE"],

        Name:                 environ["WHITE"],
        Name.Attribute:       environ["CYAN"],
        Name.Builtin:         environ["CYAN"],
        Name.Class:           environ["YELLOW"],
        Name.Function:        'bold #61AFEF',
        Name.Function.Magic:  'bold #56B6C2',
        Name.Other:           environ["CYAN"],
        Name.Tag:             environ["CYAN"],
        #Name.Tag:            '#E06C75',
        Name.Decorator:       '#61AFEF',
        Name.Variable.Class:  environ["YELLOW"],

        Error:     environ["RED"],
        String:    f'bold italic {environ["GREEN"]}',
        Number:    environ["ORANGE"],
        Operator:  environ["PURPLE"],
        Comment:   f'bold italic {environ["GRAY"]}',
    }
