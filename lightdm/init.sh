#! /usr/bin/env zsh

#wallpaper=$(sed -n /background/p ~/.dotfiles/lightdm/slick-greeter.conf | cut -d= -f2)
#wallpaper="$HOME/.dotfiles/wallpapers/space/Carina.jpg"
wallpaper=$(sed -n '/background=/p' slick-greeter.conf | cut -d= -f2)


# copy background image to recomended directory
if [ ! -f /usr/share/pixmaps/$(basename $wallpaper) ]; then
   sudo cp $wallpaper /usr/share/pixmaps/
   #sudo cp $wallpaper /usr/share/pixmaps/$(basename $wallpaper)
fi


# check if slick greeter is installed
if [ -n "$(yay -Q lightdm-slick-greeter)" ]; then
   yay -S --needed lightdm-slick-greeter
fi


sudo cp ~/.dotfiles/lightdm/lightdm.conf /etc/lightdm
sudo cp ~/.dotfiles/lightdm/slick-greeter.conf /etc/lightdm/
