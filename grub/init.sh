#! /usr/bin/env zsh

function reload_config() {
   sudo grub-mkconfig -o /boot/grub/grub.cfg
}

function reset_files() {
   sudo cp grub /etc/default/
   sudo cp 31_hold_shift /etc/grub.d/
}


case $1 in
   '--reload-config')
      reload_config
      ;;
   '--reset-files')
      reset_files
      ;;
   *)
      reset_files
      reload_config
      ;;
esac


#grubfont='/usr/share/fonts/hack-nerd-font/"Hack Regular Nerd Font Complete.ttf"'

#if [ ! -f /etc/default/gtub.bak ]; then
#   sudo cp grub.bak /etc/default/grub.bak
#fi

#sudo cp grub /etc/default/grub

# redo config:

#sudo grub-mkfont --size=14 --output=minimalTheme/hack_14.pf2 $grubfont
#sudo grub-mkfont --size=14 --output=/boot/grub/fonts/hack.pf2 $grubfont
#sudo cp -r minimalTheme /boot/grub/themes/minimalTheme
